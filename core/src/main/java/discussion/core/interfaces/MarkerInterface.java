package discussion.core.interfaces;

public class MarkerInterface implements EmptyInterface {

	public void display() {
		
		System.out.println("Hello....");
		
		if (this instanceof EmptyInterface) {
			System.out.println("Bye....");
		}
	}
	
	public static void main(String args[]) {
		
		MarkerInterface object = new MarkerInterface();
		object.display();
	}
	
}

interface EmptyInterface {
}