package discussion.core.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FinallyExample {

	public Integer finallyReturn() {

		try {
				return 10 / 0;
		}
		finally {
			return 33;
		}
	}
	
	public static void main(String args[]) {
		
		log.info("Return = " + new FinallyExample().finallyReturn());
	}
	
}