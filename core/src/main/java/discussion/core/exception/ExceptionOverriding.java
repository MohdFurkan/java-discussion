/**
 * 
 * @author MohdFurkan
 * 
	1.	If the superclass method does not declare an exception,  
		subclass overridden method cannot declare the checked exception but it can declare unchecked exception.
	2.	If the superclass method declares an exception, 
		subclass overridden method can declare same, 
		subclass exception or no exception but cannot declare parent exception.
 */

package discussion.core.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionOverriding extends ExceptionExample {

	
}

class ExceptionExample {
	
	public void nothing() {
		throw new Exception("Hello");
	}
	
}