package discussion.core.exception;

public class TryWithResoruce {
	
	public static void display(Object obj) {
		System.out.println("Object = " + obj);
	}
	
	public static void display(String obj) {
		System.out.println("String = " + obj);
	}
	
	public static void display(Integer aa) {
		System.out.println("Integer = " + aa);
	}

	public static void main(String args[]) {
		TryWithResoruce.display(null);		
	}
	
}