package discussion.core.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionPropagation {

	
	public static void first() {
		
		log.info("first()");
		second();
	}
	
	public static void second() {
	
		if (true)
				throws new Exception();
		
		log.info("first()");
	}
	
	public static void main(String arg[]) {
		ExceptionPropagation.first();
	}
	
}