/**
 * @author Mohd Furkan
 * 
 * We can create an object using newInstance method of Class.
 * But for this, our class should have a default (non-argument) constructor
 * If there is no non-argument constructor, we can not use this way
 */

package discussion.core.object.creation;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class NewInstanceOfClass {

	private Integer id;
	private String name;
	
	public static void main(String args[]) {
		
		try {
			
			Class<NewInstanceOfClass> clazz = NewInstanceOfClass.class;			
			NewInstanceOfClass object = clazz.newInstance();
			
			object.id = 20;
			object.name = "Test";
			
			log.info(object.toString());
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
		
	}
	
}