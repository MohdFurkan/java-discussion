/**
 * @author Mohd Furkan
 * 
 * Creating object using deserialization,
 * In below example first, i am writing an object to file, then reading from file.
 * Reading from file is object creation not writing
 * 
 * As we know during deserializatio constructor is not being called
 * we can see parameterized constructor is being called one while creating object not while reading that object 
 * 
 */

package discussion.core.object.creation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Deserialization {

	public static void main(String args[]) {
		
		Deserialization.doSerialize();
		Deserialization.doDeSerialize();
	}
	
	public static void doSerialize() {
		
		try (
				FileOutputStream out = new FileOutputStream("text.txt"); 
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(out)) {
			
			Employee employee = new Employee(10, "Test");
			objectOutputStream.writeObject(employee);			
		} 
		catch (Exception e) {
			log.error(e.getMessage());
		}
		
	}
	
	public static void doDeSerialize() {
		
		try (
				FileInputStream inputStream = new FileInputStream("text.txt");
				ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
			
			Employee employee = (Employee) objectInputStream.readObject();
			log.info(employee.toString());
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
}

@Data
class Employee implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5977430063674099223L;

	private Integer id;
	private String name;
	
	public Employee(Integer id, String name) {
		this.id = id;
		this.name = name;
		System.out.println("Contructor called..");
	}
	
}