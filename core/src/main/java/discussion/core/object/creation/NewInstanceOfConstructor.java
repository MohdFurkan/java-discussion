/**
 * @author Mohd Furkan
 * 
 * newInstance method of constructor class is used to create an object.
 * using this way, you can create an object using any constructors.
 * In below example we are creating two objects using non-argument and parameterized constructor
 * 
 */

package discussion.core.object.creation;

import java.lang.reflect.Constructor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor // default constructor
@AllArgsConstructor // constructor with all parameters
public class NewInstanceOfConstructor {

	private Integer id;
	private String name;
	
	public static void main(String arg[]) {
		
		try {
			
			Class clazz = NewInstanceOfConstructor.class;
			Constructor<NewInstanceOfConstructor> []constructors = clazz.getConstructors();
			
			for(Constructor<NewInstanceOfConstructor> cons: constructors) {
				
				if (cons.getParameterTypes().length > 0) {
					NewInstanceOfConstructor object2 = (NewInstanceOfConstructor) clazz.getConstructor(cons.getParameterTypes()).newInstance(1, "Test");
					log.info(object2.toString());
				} else {
					NewInstanceOfConstructor object1 = (NewInstanceOfConstructor) clazz.getConstructor(cons.getParameterTypes()).newInstance();
					object1.id = 2;
					object1.name = "Test2";
					log.info(object1.toString());
				}
			}
			
		}
		catch(Exception e) {
			log.error(e.getMessage());
		}
		
	}
	
}