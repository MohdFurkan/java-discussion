/**
 * 
 * @author Mohd Furkan
 * 
 * Your class should implement cloneable interface which is a marker interface having no constant and method
 * Constructos of the class will not be invoked while cloning
 * There are two types of cloning
 * 1. Shallow cloning
 * 2. Deep cloning
 * 
 * by default shallow cloning works
 */

package discussion.core.object.creation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@Slf4j
public class CloneExample implements Cloneable {

	private Integer id;
	private String name;
	
	public static void main(String args[]) {
		
		try {
			CloneExample object1 = new CloneExample(20, "Test");
			CloneExample object2 = object1.clone();
						
			log.info(object1.toString());
			log.info(object2.toString());
			
			log.info("Both objects are equals = " + (object1 == object2));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	@Override
	protected CloneExample clone() throws CloneNotSupportedException {
		return (CloneExample) super.clone();
	}
	
}